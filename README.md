# Clean Energy Demo

Collection of scripts for processing CORDEX simulations and other data sources. First pass at evolving towards long term goal to have comprehensive collection of scripts and procedures to better streamline and QC extracting projected changes in climate variables. Looking to archive knowledge gained about different data source quirks and tricks for dealing with these, with a framework that can hopefully be more generically applied. That is, scripts run through selecting options and such rather than just collecting snippets. 

On the current Amazon instance create a conda environment containing all the libraries needed to run the associated scripts.

`conda activate clim`

## Data acquisition

### ERA5 Reanalysis
Current download script is `pull-era5-s3.py`
Need to set variables `metprms`, `region_box` and `data_path` to match data needs.

This script makes use of the efficiency of pulling files that have been placed within the S3 system, however this doesn't represent the full possible range of dates and meteorological parameters produced for this product.
This S3 data base has stored values from the year 2008 through 2018.
*The pull script will currently download all samples within this time window unless modified*.

### Site records
Currently use what has been delivered by client. These can came in lots of formats. Maybe someday down the line might want to have a generic data base structure that individual records get entered in to, but would need to have pretty broad concept of options before could set something up that wouldn't be initially limited. 

### CORDEX simulations
These are pulled using BASH scripts provided by the data portals.  
**[Add url and additional info if needed]**

The script `spotcheck-cordex.py` takes a first pass at whether the content of these downloaded files match the what the filename suggest. 

## Pre-processing

### ERA5 Reanalysis
*As of current the reanalysis data is just used as a stand in for a demonstration so the limited pre-processing what was done as a quick set of BASH/CDO commands. These are documented in `preproc-era5.sh`. Easy enough to transfer this into a python script (using the `pycdo` library, or internally with `xarray`) at some point when have more examples of using reanalysis data.* 

### CORDEX simulations
Uses the `preproc-cordex.py` script to take the raw files that are obtained using the data-portal procedures and create single files containing regional subsets as well as performing certain aggregations that are expected to be desired for certain meteorological parameters.

*Currently have the script set up in a way that prioritises creating the minimal needed data file so as to clear out processing space. Idea is that it saves disk space and that if larger files were to be archived they would probably be the raw originals (which can then be pulled out if we want to perform a different sub-setting). If this approach changes might want to look into how this script is organized.*

Currently need to set the `data_product` and `region_lab` variables and then pass the desired meteorological parameters through the command line; e.g., `$ python precproc-cordex.py uas vas`.

Plan is to extend the dictionaries
```
region_boxes = {'NorthSea' : '-10,12,49,62'}
product_keys = {'eucordex' : 'EUR-11'}
```
as use cases get extended so that can just set or pass the `data_product` and `region_lab` to get a standard result. 

## Analysis

### Climatology Differences
Use the script `$python clim-diffs.py [METPRM]` to look at changes between different climatology periods within the individual simulations and over the ensemble. *Time periods are currently hard coded within the function `set_values()`, will want these to be adjustable in future. Also `set_values()` was probably a bad name for this function, if anyone has something they like better please change it.* General layout is as a collection of functions that can then apply as desired; e.g., deciding to whether to set `calc_annaul = True` to use annual means or to allow the default of a monthly climatology.

### Wind Roses
These plots are created with `rosenkerne.py`.

### Site specific studies
A Jupyter-Notebook outlining ideas for site specific evaluation is given by: `sketch-site-study.ipynb`. To create the graphics outlined in the notebook using samples from all available simulations can use `windspeed-site-study.py`.

### Circulation field comparison
A Jupyter-Notebook outlining creating plots that compare contemporary circulation regimes outlined in a reanalysis product to those in the simulations is given by:  `sketch-stream-plot.ipynb`. 
  
