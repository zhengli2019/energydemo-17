''' ERA5 values from S3
    ===================

    A sketch of extracting ERA5 fields from their S3 storage location.
    There's a limited amount of fields, time ranges, etc, available. However
    if running the scrip on an Amazon instance then the download speeds 
    between S3 nodes is better than would typically see when accessing the 
    ECMWF servers. 
                                                                             '''
import itertools
import xarray as xr
import botocore
import boto3
import datetime
import os

# ::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>:: #

metprms = [
    'eastward_wind_at_10_metres',
    'northward_wind_at_10_metres'
] 

''' Have to download the full file, then chop a region out of it. 
                                                                             '''
region_box = {
    'min_lat' :  46.0,
    'max_lat' :  65.0,
    'min_lon' : -19.0,
    'max_lon' :  14.0
}

''' State and move to the directory want files to be stored in.
                                                                             '''
data_path = '/home/ubuntu/era5'
os.chdir(data_path)

# ::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>::x::<>:: #


era5_bucket = 'era5-pds'
#/No AWS keys required/#
client = boto3.client(
    's3',config=botocore.client.Config(
        signature_version=botocore.UNSIGNED))

''' Need to select the years and months to download. Current S3
    repository has years from 2008 to 2018
                                                                             '''
years = range(2008,2019) 
months = range(1,13) 
for y,m in itertools.product(years,months) :
    date = datetime.date(y,m,1)

    for metprm in metprms : 

        # ... file path patterns ...
        s3_data_ptrn = '{year}/{month}/data/{metprm}.nc'
        data_file_ptrn = '{year}{month}_{metprm}.nc'

        year = date.strftime('%Y')
        month = date.strftime('%m')

        s3_data_key = s3_data_ptrn.format(
            year=year, month=month, metprm=metprm)
        data_file = data_file_ptrn.format(
            year=year, month=month, metprm=metprm)

        if not os.path.isfile(data_file): # check if file already exists
            print("Downloading %s from S3..." % s3_data_key)
            client.download_file(era5_bucket, s3_data_key, data_file)

        ds = xr.open_dataset(data_file)
        #ds = ds.load()
        
        ''' The simulation grid is reported with longitude units in the 0:360
            range, which makes it hard to define a continuous box from
            west to east near the prime meridian. So switch
            things. The difficult part is that once relabel the
            longitude values then need to resort the values. This
            requires loading the whole field into memory, which can
            exceed memory restrictions on some machines. To work
            around that first subset the region, write out the file,
            then reload the smaller file in and sort along the indexes
            from there.
                                                                             '''
        lon = ds.lon.values
        lon[lon>180.0] = lon[lon>180.0]-360.0
        ds.lon.values = lon
        lat = ds.lat.values
        ds = ds.loc[dict(
            lon=lon[(lon>=region_box['min_lon'])
                    & (lon<=region_box['max_lon'])],
            lat=lat[(lat>=region_box['min_lat'])
                    & (lat<=region_box['max_lat'])])]

        os.remove(data_file)
        ds.to_netcdf(
            'tmp_'+metprm+'_'+str(y)+'-'+str(m).zfill(2)+'.nc')
        ds = xr.open_dataset(
            'tmp_'+metprm+'_'+str(y)+'-'+str(m).zfill(2)+'.nc')
        ds = ds.sortby('lon')
        ds.to_netcdf(
            metprm+'_'+str(y)+'-'+str(m).zfill(2)+'.nc')
        os.remove(
            'tmp_'+metprm+'_'+str(y)+'-'+str(m).zfill(2)+'.nc')

